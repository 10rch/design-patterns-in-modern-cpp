#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <sstream>

int main()
{
	std::vector<std::string> names { "john", "jane", "jill", "jack"};
	std::vector<std::string>::iterator it = names.begin(); // std::begin(names) - works with arrays
	std::cout << "First name is " << *it << std::endl;

	++it;
	it->append(std::string{" doe"});
	std::cout << "Second name is " << *it << std::endl;

	while(++it != names.end()){
		std::cout << "Another name: " << *it << std::endl;
	}

	for(auto ri = names.rbegin(); ri != names.rend(); ++ri){
		std::cout << *ri;
		if(ri + 1 != names.rend())
			std::cout << ", ";
	}
	std::cout << std::endl;

	//begin and end needs to be implemented to use range based for loop
	for(auto& name : names){
		std::cout << "name = " << name << std::endl;
	}

	return 0;
}