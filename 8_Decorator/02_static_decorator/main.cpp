
#include <iostream>
#include <string>
#include <vector>
#include <sstream>


struct Shape
{
	virtual std::string str() const = 0;
};

struct Circle : Shape
{
	float radius;
	Circle(){}
	Circle(float radius) : radius(radius) {}

	void resize(float factor){
		radius *= factor;
	}

	std::string str() const override {
		std::ostringstream oss;
		oss << "A circle of radius " << radius;
		return oss.str();
	}
};

struct Square : Shape
{
	float side;
	Square() {}
	Square(float side) : side(side) {}

	std::string str() const override {
		std::ostringstream oss;
		oss << "A square with side " << side;
		return oss.str();
	}
};

//mixin inheritance
template<typename T> struct ColoredShape2 : T
{
	static_assert(std::is_base_of<Shape, T>::value, "Template argument must be a Shape");

	std::string color;

	ColoredShape2() {}

	template <typename ... Args>
	ColoredShape2(const std::string& color, Args ...args)
		: T(std::forward<Args>(args)...) , color(color) {}

	std::string str() const override {
		std::ostringstream oss;
		oss << T::str() << " has the color " << color;
		return oss.str();
	}
};

template<typename T> struct TransparentShape2 : T
{
	static_assert(std::is_base_of<Shape, T>::value, "Template argument must be a Shape");

	uint8_t transparency;


	template <typename ... Args>
	TransparentShape2(const uint8_t transparency, Args ...args)
			: T(std::forward<Args>(args)...) , transparency(transparency) {}

	std::string str() const override {
		std::ostringstream oss;
		oss << T::str() << " has "
			<< static_cast<float>(transparency) / 255.f * 100.f
			<< "% transparency";
		return oss.str();
	}
};

int main()
{
	ColoredShape2<Circle> green_circle{"green", 5};
	green_circle.resize(2);
	std::cout << green_circle.str() << std::endl;

	TransparentShape2<ColoredShape2<Square>> square{51, "blue", 10};
	std::cout << square.str() << std::endl;

	return 0;
}
