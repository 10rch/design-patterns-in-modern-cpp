#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <sstream>
#include <sstream>
#include <memory>

enum class OutputFormat
{
	markdown,
	html
};

struct ListStrategy
{
	virtual void start(std::ostringstream& oss) {}
	virtual void add_list_item(std::ostringstream& oss, const std::string& item) = 0;
	virtual void end(std::ostringstream& oss) {}
};

struct MarkdownListStrategy : ListStrategy{
	void add_list_item(std::ostringstream &oss, const std::string &item) override {
		oss << "* " << item << "\n";
	}
};

struct HtmlListStrategy : ListStrategy{
	void start(std::ostringstream &oss) override {
		oss << "<ul>\n";
	}

	void add_list_item(std::ostringstream &oss, const std::string &item) {
		oss << "  <li>" << item << "</li>\n";
	}

	void end(std::ostringstream &oss) override {
		oss << "</ul>\n";

	}
};

struct TextProcessor
{
	std::string str(){
		return oss.str();
	}

	void clear(){
		oss.str("");
		oss.clear();
	}

	void append_list(const std::vector<std::string>& items){
		list_strategy->start(oss);
		for (auto& item : items) {
			list_strategy->add_list_item(oss, item);
		}
		list_strategy->end(oss);
	}

	void set_output_format(const OutputFormat& format){
		switch (format) {
			case OutputFormat::markdown:
				list_strategy = std::make_unique<MarkdownListStrategy>();
				break;
			case OutputFormat::html:
				list_strategy = std::make_unique<HtmlListStrategy>();
				break;
		}
	}
private:
	std::ostringstream oss;
	std::unique_ptr<ListStrategy> list_strategy;
};

int main()
{
	std::vector<std::string> items{"foo", "bar", "baz"};

	TextProcessor tp;
	tp.set_output_format(OutputFormat::markdown);
	tp.append_list(items);
	std::cout << tp.str() << "\n";

	tp.clear();
	tp.set_output_format(OutputFormat::html);
	tp.append_list(items);
	std::cout << tp.str() << "\n";


	return 0;
}