#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <cmath>

using namespace std;

enum class PointType
{
	cartesian,
	polar
};

struct PointFactory;

class Point{
	Point(float x, float y) : x(x), y(y) {}
	float x, y;

	class PointFactory{
		PointFactory() {}
	public:
		static Point NewCartesian(float x, float y){
			return  {x, y};
		}

		static Point NewPolar(float r, float theta){
			return {r * cos(theta), r * sin(theta)};
		}
	};
public:

	friend ostream &operator<<(ostream &os, const Point &point) {
		os << "x: " << point.x << " y: " << point.y;
		return os;
	}
	static PointFactory Factory;
};



int main()
{
//	will not work
//	Point p1{1,2};
//	Point::PointFactory pf;

//	works only if PointFactory is public
//	auto p = Point::PointFactory::NewPolar(1, 2);

//	better use this
	auto p = Point::Factory.NewPolar(1, 2);
	cout << p << endl;


	return 0;
}