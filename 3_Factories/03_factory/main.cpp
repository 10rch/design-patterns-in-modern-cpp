#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <cmath>

using namespace std;

enum class PointType
{
	cartesian,
	polar
};

struct PointFactory;

class Point{
public:
//	friend class PointFactory; //breaking open-closed principle

	Point(float x, float y) : x(x), y(y) {}
public:
	float x, y;

	friend ostream &operator<<(ostream &os, const Point &point) {
		os << "x: " << point.x << " y: " << point.y;
		return os;
	}
};

struct PointFactory{
	static Point NewCartesian(float x, float y){
		return  {x, y};
	}

	static Point NewPolar(float r, float theta){
		return {r * cos(theta), r * sin(theta)};
	}
};

int main()
{
	auto p = PointFactory::NewPolar(1, 2);

	cout << p << endl;

	return 0;
}