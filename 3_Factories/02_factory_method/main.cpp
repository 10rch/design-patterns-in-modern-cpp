#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <cmath>

using namespace std;

enum class PointType
{
	cartesian,
	polar
};

class Point{
public:
	Point(float x, float y) : x(x), y(y) {}
public:
	float x, y;

	static Point NewCartesian(float x, float y){
		return  {x, y};
	}

	static Point NewPolar(float r, float theta){
		return {r * cos(theta), r * sin(theta)};
	}

	friend ostream &operator<<(ostream &os, const Point &point) {
		os << "x: " << point.x << " y: " << point.y;
		return os;
	}
};

int main()
{
	auto p = Point::NewPolar(1, 2);

	cout << p << endl;

	return 0;
}