#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <sstream>
#include <variant>

//struct House
//{
//	std::variant<std::string, int> house;
//	house = "Montefiore Castle";
//};

struct AddressPrinter
{
	void operator()(const std::string& house_name) const{
		std::cout << "A house called " << house_name << "\n";
	}
	void operator()(const int house_number) const{
		std::cout << "House number " << house_number << "\n";
	}
};

int main()
{
	std::variant<std::string, int> house;
	house = "Montefiore Castle";

	AddressPrinter ap;
	std::visit(ap, house);

	return 0;
}