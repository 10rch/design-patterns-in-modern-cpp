
#include <iostream>
#include <string>
#include <vector>
#include <boost/bimap.hpp>
#include <boost/flyweight.hpp>

typedef uint32_t key;

struct User
{
	User(const std::string& first_name, const std::string& last_name)
	: first_name(add(first_name)), last_name(add(last_name)) {}

	const std::string get_first_name() const{
		return names.left.find(first_name)->second;
	}

	const std::string get_last_name() const{
		return names.left.find(last_name)->second;
	}

	friend std::ostream &operator<<(std::ostream &os, const User &user);

protected:
	key first_name, last_name;
	static boost::bimap<key, std::string> names;
	static key seed;

	static key add(const std::string& s){
		auto it = names.right.find(s);
		if(it == names.right.end()){
			key id = ++seed;
			names.insert({seed, s});
			return id;
		}
		return it->second;
	}
};

std::ostream &operator<<(std::ostream &os, const User &user) {
	os << "first_name: " << user.get_first_name() << " last_name: " << user.get_last_name()
	<< "(" << user.last_name << ")";
	return os;
}

key User::seed{0};
boost::bimap<key, std::string> User::names{};

struct User2
{
	boost::flyweight<std::string> first_name, last_name;

	User2(const std::string& first_name, const std::string& last_name)
	: first_name(first_name), last_name(last_name) {}
};

int main(int argc, char* argv[])
{
	User2 user1{"John", "Smith"};
	User2 user2{"Jane", "Smith"};

	std::cout << user1.first_name << std::endl << user2.first_name << std::endl;

	std::cout << std::boolalpha;

	std::cout << (&user1.first_name.get() == &user2.first_name.get()) << std::endl;
	std::cout << (&user1.last_name.get() == &user2.last_name.get()) << std::endl;
	return 0;
}
