#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <sstream>
#include <boost/signals2.hpp>

template <typename T>
struct Observable2
{
	boost::signals2::signal<void(T&, const std::string&)> field_changed;
};

class Person2 : public Observable2<Person2>
{
	int age;
public:
	Person2(int age) : age(age) {}

	int get_age() const {
		return age;
	}

	void set_age(int age){
		if(this->age == age) return;
		this->age = age;
		field_changed(*this, "age");
	}
};


int main()
{
	Person2 p2{10};

	auto conn = p2.field_changed.connect(
			[](Person2& p, const std::string& field_name){
				std::cout << field_name << " has changed\n";
			}
	);
	p2.set_age(20);
	conn.disconnect();



	return 0;
}