#pragma once

#include <string>
#include <vector>
#include <algorithm>
#include <mutex>

template <typename> struct Observer;

template <typename T>
class SaferObservable
{
	std::vector<Observer<T>*> observers;
	typedef std::recursive_mutex mutex_t;
	mutex_t mtx;
public:
	void notify(T& source, const std::string& field_name){
		std::scoped_lock<mutex_t> lock{mtx};
		for (auto observer : observers)
			observer->field_changed(source, field_name);
	}

	void subscribe(Observer<T>& observer){
		std::scoped_lock<mutex_t> lock{mtx};
		observers.push_back(&observer);
	}

	void unsubscribe(Observer<T>& observer){
		auto it = std::find(observers.begin(), observers.end(), &observer);
		if(it != observers.end())
			*it = nullptr;
//		std::scoped_lock<mutex_t> lock{mtx};
//		observers.erase(
//				std::remove(observers.begin(), observers.end(), &observer),
//				observers.end()
//		);
	}
};
