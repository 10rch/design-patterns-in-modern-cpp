#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <sstream>
#include "Observer.hpp"
#include "Observable.hpp"

class Person : public Observable<Person>//observable
{
	int age;
public:
	explicit Person(int age) : age(age) {}

	int get_age() const {
		return age;
	}

	void set_age(int age){
		if(this->age == age) return;
		this->age = age;
		notify(*this, "age");
	}
};

//observer & observable
struct ConsolePersonObserver
		: public Observer<Person>
{
private:
	void field_changed(Person &source, const std::string &field_name) override {
		std::cout << "Person's " << field_name << " has changed to ";
		if(field_name == "age") std::cout << source.get_age();
		std::cout <<".\n";
	}
};


int main()
{
	Person person{10};
	ConsolePersonObserver cpo;
	person.subscribe(cpo);

	person.set_age(11);
	person.set_age(12);

	person.unsubscribe(cpo);
	person.set_age(12);


	return 0;
}