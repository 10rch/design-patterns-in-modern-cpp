#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <fstream>

using namespace std;

struct Document;

// without segregation

// struct IMachine
// {
// 	virtual void print(Document& doc) = 0;
// 	virtual void scan(Document& doc) = 0;
// 	virtual void fax(Document& doc) = 0;
// };

//segregation to smaller interfaces
struct IPrinter
{
	virtual void print(Document& doc) = 0;
};

struct IScanner
{
	virtual void scan(Document& doc) = 0;
};

struct IFax
{
	virtual void fax(Document& doc) = 0;
};

struct IMachine : IScanner, IPrinter {};

// struct MFP : IMachine
// {
// 	void print(Document &doc) override {}
// 	void scan(Document &doc) override {}
// 	void fax(Document &doc) override {}
// };

// struct Scanner : IMachine
// {
// 	void print(Document &doc) override {}
// 	void scan(Document &doc) override {}
// 	void fax(Document &doc) override {}
// };


struct Printer : IPrinter
{
	void print(Document &doc) override {}
};

struct Scanner : IScanner
{
	void scan(Document &doc) override {}
};

struct Machine : IMachine
{
	IPrinter& printer;
	IScanner& scanner;

	Machine(IPrinter& printer, IScanner& scanner) : printer(printer), scanner(scanner) {}
	void print(Document &doc) override {
		printer.print(doc);
	}
	void scan(Document &doc) override {
		scanner.scan(doc);
	}


};



int main(){

	return 0;
}