
#include <iostream>
#include <string>
#include <vector>
#include <memory>

struct BankAccount
{
	int balance{0};
	int overdraft_limit{-500};

	void deposit(int amount){
		balance += amount;
		std::cout << "Deposited " << amount << ", balance is now " << balance << std::endl;
	}

	void withdraw(int amount){
		if(balance - amount >= overdraft_limit){
			balance -= amount;
			std::cout << "Withdrew " << amount << ", balance is now " << balance << std::endl;
		}
	}

	friend std::ostream &operator<<(std::ostream &os, const BankAccount &account) {
		os << "balance: " << account.balance;
		return os;
	}
};

struct Command
{
	virtual void call() = 0;
};

struct BankAccountCommand : Command
{
	BankAccount& account;
	enum Action { deposit, withdraw } action;
	int amount;

	BankAccountCommand(BankAccount &account, Action action, int amount) : account(account), action(action),
																		  amount(amount) {}

	void call() override {
		switch (action) {
			case deposit:
				account.deposit(amount);
				break;
			case withdraw:
				account.withdraw(amount);
				break;
		}
	}
};


int main()
{
	BankAccount ba;
	std::vector<BankAccountCommand> commands{
		BankAccountCommand{ba, BankAccountCommand::deposit, 100},
		BankAccountCommand{ba, BankAccountCommand::withdraw, 50}
	};

	std::cout << ba << std::endl;

	for (auto& cmd : commands) {
		cmd.call();
	}

	return 0;
}
