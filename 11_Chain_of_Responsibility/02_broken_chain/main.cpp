
#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <boost/signals2.hpp>

//event broker = cor + observer (signals2) + cqs

struct Query
{
	std::string creature_name;
	enum Argument { attack, defence } argument;
	int result;

	Query(const std::string &creatureName, Argument argument, int result) : creature_name(creatureName),
																			argument(argument), result(result) {}
};

struct Game //mediator
{
	boost::signals2::signal<void(Query&)> queries;
};

struct Creature
{
	Game& game;
	int attack, defence;
	std::string name;
public:
	Creature(Game &game, int attack, int defence, const std::string &name) : game(game), attack(attack),
																			 defence(defence), name(name) {}

	int get_attack() const{
		Query q{name, Query::Argument::attack, attack};
		game.queries(q);
		return q.result;
	}

	friend std::ostream &operator<<(std::ostream &os, const Creature &creature) {
		os << "attack: " << creature.get_attack() << " name: " << creature.name;
		return os;
	}
};

class CreatureModifier
{
	Game& game;
	Creature& creature;
public:
	CreatureModifier(Game &game, Creature &creature) : game(game), creature(creature) {}
	virtual ~CreatureModifier() = default;
};

class DoubleAttackModifier : public CreatureModifier
{
	boost::signals2::connection conn;
public:
	DoubleAttackModifier(Game &game, Creature &creature) : CreatureModifier(game, creature) {
		conn = game.queries.connect([&](Query& q){
			if(q.creature_name == creature.name && q.argument == Query::Argument::attack){
				q.result *= 2;
			}
		});
	}
	~DoubleAttackModifier() { conn.disconnect(); }
};

int main(int ac, char* av[])
{
	Game game;
	Creature goblin{ game, 2, 2, "Strong Goblin"};
	std::cout << goblin << std::endl;

	{
		DoubleAttackModifier dam{game, goblin};
		std::cout << goblin << std::endl;
	}

	std::cout << goblin << std::endl;

	return 0;
}
