#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <sstream>

#include "Person.hpp"
#include "ChatRoom.hpp"

int main()
{
	ChatRoom room;
	Person john{"john"};
	Person jane{"jane"};

	room.join(&john);
	room.join(&jane);

	john.say("hi room");
	jane.say("oh, hey john");

	Person simon{"simon"};
	room.join(&simon);
	simon.say("hi everyone");

	jane.pm("simon", "Glad you found us, simon!");

	return 0;
}