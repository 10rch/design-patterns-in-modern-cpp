#pragma once


struct ChatRoom
{
	std::vector<Person*> people;

	void broadcast(const std::string& origin,
				   const std::string& message);

	void join(Person* p);
	void message(const std::string origin,
				 const std::string& who,
				 const std::string& message)
	{
		auto target = std::find_if(people.begin(), people.end(),
								   [&](const Person* p){
			return p->name == who;
		});
		if(target != people.end()){
			(*target)->recieve(origin, message);
		}
	}
};
