#include <iostream>
#include <cstdio>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <memory>
#include "Person.hpp"
#include "PersonBuilder.hpp"
#include "PersonAddressBuilder.hpp"
#include "PersonJobBuilder.hpp"

using namespace std;



int main()
{
    Person p = Person::create()
            .lives().at("street").with_postcode("123AS").in("London")
            .works().at("IT").as_a("Developer").earning(10e6);

	cout << p << endl;

	return 0;
}