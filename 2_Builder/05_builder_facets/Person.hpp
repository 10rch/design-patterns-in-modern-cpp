#pragma once

#include <string>
#include <ostream>

class PersonBuilder;

class Person
{
	//address
	std::string street_adress, post_code, city;

	//employment
	std::string company_name, position;
	int annual_income{0};
public:
	static PersonBuilder create();

	friend std::ostream &operator<<(std::ostream &os, const Person &person);

	friend class PersonBuilder;
	friend class PersonJobBuilder;
	friend class PersonAddressBuilder;
};