cmake_minimum_required(VERSION 3.24)

project(proj)

SET(CMAKE_INCLUDE_PATH ${CMAKE_INCLUDE_PATH} "C:/Users/1NR_Operator_9/source/libraries/boost_1_81_0")
SET(CMAKE_LIBRARY_PATH ${CMAKE_LIBRARY_PATH} "C:/Users/1NR_Operator_9/source/libraries/boost_1_81_0/libs")

find_package(Boost COMPONENTS serialization REQUIRED)
IF (Boost_FOUND)
    message(STATUS "Boost has been found")
    message("Boost libraries " ${Boost_INCLUDE_DIR})
    INCLUDE_DIRECTORIES(${Boost_INCLUDE_DIR})
    ADD_DEFINITIONS( "-DHAS_BOOST" )
ENDIF()

add_executable(${PROJECT_NAME} main.cpp)

message(STATUS ${Boost_LIBRARIES})

target_link_libraries(${PROJECT_NAME} ${Boost_LIBRARIES} )