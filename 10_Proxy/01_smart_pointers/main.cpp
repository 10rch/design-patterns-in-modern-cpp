
#include <iostream>
#include <string>
#include <vector>
#include <memory>


struct BankAccount
{
	virtual ~BankAccount() = default;
	virtual void deposit(int amount) = 0;
	virtual void withdraw(int amount) = 0;
};

struct CurrentAccount : BankAccount
{


	explicit CurrentAccount(int balance) : balance(balance) {}

	void deposit(int amount) override{
		balance += amount;
	}

	void withdraw(int amount) override {
		if(amount <= balance) balance -= amount;
	}

	friend std::ostream &operator<<(std::ostream &os, const CurrentAccount &obj) {
		return os  << " balance: " << obj.balance;
	}

private:
	int balance;
};

int main()
{
	BankAccount* a = new CurrentAccount(123);
	a->deposit(321);
	delete a;

	auto b = std::make_shared<CurrentAccount>(123);
	BankAccount* actual = b.get();

	return 0;
}
