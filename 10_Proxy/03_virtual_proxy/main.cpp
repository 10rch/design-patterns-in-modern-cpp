
#include <iostream>
#include <string>
#include <vector>
#include <memory>

struct Image
{
	virtual void draw() = 0;
};

struct  Bitmap : Image
{
	Bitmap(const std::string& filename){
		std::cout << "Loading bitmap from " << filename << std::endl;
	}

	void draw() override {
		std::cout << "Drawing bitmap" << std::endl;
	}
};

struct LazyBitmap : Image
{
	LazyBitmap(const std::string &filename) : filename(filename) {}

	void draw() override {
		if(!bmp)
			bmp = new Bitmap(filename);
		bmp->draw();
	}

private:
	std::string filename;
	Bitmap *bmp{nullptr};
};

int main()
{
	LazyBitmap bmp{"pokemon.png"};
	bmp.draw();

	return 0;
}
